//Demo Sequelize Connection
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('master', 'sa', 'password_01', {
    host: '192.168.0.175',
    dialect: 'mssql',
    trustServerCertificate: true
  });

async function connect(){
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

connect();