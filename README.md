Untuk menjalankan Docker, bisa ikuti petunjuk dari web Tedious:
https://tediousjs.github.io/tedious/installation.html

Atau jalankan perintah berikut ini:
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=password_01" -p 1433:1433 -v sqldatavol:/var/opt/mssql -d mcr.microsoft.com/mssql/server:2019-latest

Jika mau lihat log-nya, bisa jalankan perintah:
docker logs -tf <nama_container>